let tab = document.querySelectorAll(`.tabs-title`),
tabContent = document.querySelectorAll(`.tab`),
tabName;

tab.forEach(item => {
    item.addEventListener(`click`, selectTabNav);
});
function selectTabNav() {
    tab.forEach(item => {
        item.classList.remove(`active`);
    });
    this.classList.add(`active`);
    tabName = this.getAttribute(`data-tab`);
    tabsN(tabName);
}
function tabsN(tabName) {
    tabContent.forEach(item => {
        item.classList.contains(tabName) ? item.classList.add(`active`) : item.classList.remove(`active`);
    })
}