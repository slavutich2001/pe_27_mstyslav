const picture = document.querySelectorAll(`img`);
let giveMeImage = 0;
let movingSlider = false;
function slider() {
    picture[giveMeImage].className = "image-to-show";
    giveMeImage = (giveMeImage + 1) % picture.length;
    picture[giveMeImage].classList.add("active");
}

//////////////////////////////////////////// START


const startSlider = document.querySelector(".start_button");
startSlider.addEventListener("click", nextImage);
function nextImage() {
    if (!movingSlider) {
        sliderShow = setInterval(slider, 3000, (movingSlider = true));
    }
}

//////////////////////////////////////////// STOP

let sliderShow = setInterval(slider, 3000, (movingSlider = true));
const stopPicture = document.querySelector(".stop_button");
stopPicture.addEventListener("click", stopSlider);
function stopSlider() {
    clearInterval(sliderShow);
    movingSlider = false;
}


