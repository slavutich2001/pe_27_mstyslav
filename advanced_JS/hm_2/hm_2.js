const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.getElementById(`root`);

books.forEach(item => {
    if (item.hasOwnProperty(`author`) && item.hasOwnProperty(`name`) && item.hasOwnProperty(`price`)){
       const ul = document.createElement(`ul`)
        ul.innerHTML = `<li>${item.author}</li> <li>${item.name}</li> <li>${item.price}</li>`;
       root.append(ul)
    }
    try {
        if (!item.hasOwnProperty('author')){
             throw new Error('property author is not defined')
        }
        if (!item.hasOwnProperty('name')){
            throw new Error('property name is not defined')
        }
        if (!item.hasOwnProperty('price')){
            throw new Error('property price is not defined')
        }
    }
    catch (error){
        console.log(error.message);
    }
})



