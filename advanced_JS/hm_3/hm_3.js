class Employee {
    constructor(options) {
        this.name = options.name;
        this.age = options.age;
        this.salary = options.salary;
    }
    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }
    get age() {
        return this._age;
    }
    set age(age) {
        this._age = age;
    }
    get salary() {
        return this._salary;
    }
    set salary(salary) {
        this._salary = salary;
    }
}

const employee = new Employee({
    name: 'Jane',
    age: 28,
    salary: 1000,
});
employee.name = 'Fernando';
employee.age = 33;
employee.salary = 2000;
console.log(employee);
class Programmer extends Employee {
    constructor(options) {
        super(options);
        this.lang = options.lang;
    }
    get salary() {
        return super.salary * 3;
    }
    set salary(salary) {
        super.salary = salary;
    }
}
const programmerSecond = new Programmer({
    name: 'Justin',
    age: 40,
    salary: 15000,
    lang: 'fr',
});
console.log(programmerSecond);

const programmerThird = new Programmer({
    name: 'Mike',
    age: 18,
    salary: 2000,
    lang: 'eng',
});
console.log(programmerThird);