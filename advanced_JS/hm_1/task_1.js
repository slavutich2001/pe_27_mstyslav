const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const clients3 = [...clients1, ...clients2 ]
const resultArr = []
clients3.forEach(item => {
    if (!resultArr.includes(item)) resultArr.push(item)
})
console.log(resultArr);