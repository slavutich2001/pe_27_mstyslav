let btn = document.querySelector('.button');

btn.onclick= () => {
    document.body.classList.toggle(`default`);
    document.body.classList.toggle(`dark`);
    localStorage.theme = document.body.className || `default`
}

if (!localStorage.theme) localStorage.theme = `default`;
document.body.className = localStorage.theme ;
