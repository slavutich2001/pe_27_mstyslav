let number = document.getElementById(`number`);
let text = document.createElement(`p`);
text.classList.add(`p`);
text.textContent = `X`;
let span = document.createElement(`span`);
span.classList.add(`span`);
number.addEventListener(`focus`, () => {
    number.style.border = '5px solid green';
});
number.addEventListener("blur", () => {
    if (number.value < 0 || number.value === '') {
        number.style.border = `5px solid red`;
        document.querySelector(`label`).append(text);
        text.innerText = `Please enter correct price`;
        text.style.color = `red`
        span.remove();
    } else {
        document.querySelector(`label`).before(span);
        span.innerHTML = `Текущая цена: ${number.value} <button class="button_X">X</button>`;
        span.style.color = `green`
        number.style.color = `green`
        text.remove();

        text.addEventListener(`click`, () => {
            span.remove();
            number.value = '';
        })
    }
});